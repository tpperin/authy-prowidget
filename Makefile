TARGET = :clang
ARCHS = armv7 armv7s arm64
THEOS_PACKAGE_DIR_NAME = pkgs
THEOS_DEVICE_IP=192.168.1.52
DEBUG=0
ADDITIONAL_CFLAGS=-fobjc-arc

include theos/makefiles/common.mk

BUNDLE_NAME = Authy
Authy_FILES = Authy.m ViewController.m
Authy_FRAMEWORKS = Foundation UIKit CoreGraphics QuartzCore
Authy_LIBRARIES = prowidgets objcipc
Authy_INSTALL_PATH = /Library/ProWidgets/Widgets/
Authy_BUNDLE_EXTENSION = widget

include $(THEOS_MAKE_PATH)/bundle.mk
SUBPROJECTS += authysubstrate
include $(THEOS_MAKE_PATH)/aggregate.mk

before-stage::
	find . -name ".DS_Store" -delete
after-install::
	install.exec "killall -9 backboardd"
