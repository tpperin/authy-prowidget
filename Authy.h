//
//  Authy.h
//  Authy ProWidget
//
//  Created by Tim Perin on 05.05.2014.
//  Copyright (c) 2014 Tim Perin. All rights reserved.
//

#import "headers.h"
#import "interface.h"
#import "ViewController.h"

@interface PWWidgetAuthy : PWWidget {
	PWWidgetAuthyViewController *_viewController;
}

@end