//
//  Authy.m
//  Authy ProWidget
//
//  Created by Tim Perin on 05.05.2014.
//  Copyright (c) 2014 Tim Perin. All rights reserved.
//

#import "Authy.h"

@implementation PWWidgetAuthy

- (void)load {
	//Check if 1Password is installed, dismiss if it isn't
	SBApplicationController *controller = [objc_getClass("SBApplicationController") sharedInstance];
	SBApplication *app = [controller applicationWithDisplayIdentifier:AppIdent];
	if (app == nil) {
		[self showMessage:@"You need to have Authy installed to use this app."];
		[self dismiss];
		return;
	}

	//Create and push view controller
	_viewController = [[PWWidgetAuthyViewController alloc] initForWidget:self];
	[self pushViewController:_viewController animated:NO];
}

- (BOOL)requiresProtectedDataAccess {
	return YES;
}


@end