#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import <libprowidgets/libprowidgets.h>
#import <libobjcipc/objcipc.h>

#define AppIdent @"com.authy"

#if 0
#define DEBUGLOG(x, ...) NSLog(x, ## __VA_ARGS__);
#else
#define DEBUGLOG(x, ...)
#endif