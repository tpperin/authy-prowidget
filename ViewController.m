//
//  ViewController.m
//  Authy ProWidget
//
//  Created by Tim Perin on 05.05.2014.
//  Copyright (c) 2014 Tim Perin. All rights reserved.
//

#import "ViewController.h"

@implementation PWWidgetAuthyViewController

@synthesize filteredItems, items;

- (void)load {
	//Setup
	self.shouldAutoConfigureStandardButtons = YES;
	self.requiresKeyboard = NO;
	self.title = @"Authy";
	self.actionButtonText = @"Manage";

	//Set table delegates
	self.tableView.delegate = self;
    self.tableView.dataSource = self;

    //Create search bar and add as table header
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    searchBar.delegate = self;
    self.tableView.tableHeaderView = searchBar;
   	//[self.view addSubview:searchBar];

    //Add activity indicator while loading
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	loading.frame = CGRectMake(125.0,125.0,50.0,50.0);
	loading.tag = 1;
	[self.view addSubview:loading];

	//Action button handler opens the 1Password App
	[self setActionEventHandler:self selector:@selector(manage)];
	[self setHandlerForEvent:[PWContentViewController titleTappedEventName] target:self selector:@selector(titleTapped)];

	//Create array to hold items filtered from search bar
    filteredItems = [[NSMutableArray alloc] init];

    //Start the process
	[self getList];
	[self performSelector:@selector(timerTick) withObject:nil afterDelay:1.0 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

- (void)titleTapped {
	[self getList];
}

- (void)manage {
	SpringBoard *app = (SpringBoard *)[UIApplication sharedApplication];
	[app launchApplicationWithIdentifier:AppIdent suspended:NO];
	[self.widget dismiss];
}

- (void)getList {
	//Start activity indicator
	[((UIActivityIndicatorView*)[self.view viewWithTag:1]) startAnimating];

	//Send IPC message to 1Pass saying we want to get the list of accounts
	NSDictionary *dict = @{ @"action": @"getList"};
	[OBJCIPC sendMessageToAppWithIdentifier:AppIdent messageName:@"Authy" dictionary:dict replyHandler:^(NSDictionary *reply) {
		BOOL success = [reply[@"success"] boolValue];
		if (success) {
			//We successfully got the items
			if (items && [items count] > 0) {
				if ([items[0][1] isEqual:dict[@"reply"][0][1]]) {
					[self getList];
					return;
				}
			}
			if ([reply[@"items"] count] == 0) {
				[self.widget showMessage:@"No items found. Open Authy and add accounts"];
				[self.widget dismiss];
				return;
			}
			items =  [reply[@"items"] mutableCopy];
			filteredItems = [items mutableCopy];
			
			self.title = @"Authy - 20";

			[self reload];
		}
		else {
			//Didn't get items, means we need to login to 1Password
			[self.widget showMessage:@"There was an error getting accounts"];
			[self.widget dismiss];
		}
	}];
}

-(void)timerTick {
	NSArray *sp = [self.title componentsSeparatedByString:@" - "];
	if (!self.widget.isPresenting || sp == nil || [sp count] != 2) return;

	int curNum = [sp[1] integerValue];
	if (curNum > 0) {
		self.title = [NSString stringWithFormat:@"Authy - %i",(curNum - 1)];
	}
	else {
		self.title = @"Authy";
		[self getList];
	}
	[self performSelector:@selector(timerTick) withObject:nil afterDelay:1.0 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
	[((UIActivityIndicatorView*)[self.view viewWithTag:1]) startAnimating];

	if ([filteredItems count] > 0) {
		[filteredItems removeAllObjects];
	}
	if (searchText.length == 0) {
		filteredItems = [items mutableCopy];
		[searchBar resignFirstResponder];
		[self reload];
		return;
	}
    for (NSArray *item in items) {
        NSRange range = [item[0] rangeOfString:searchText
                                      options:NSCaseInsensitiveSearch];
        
        if (range.location != NSNotFound)
            [filteredItems addObject:item];
    }
    
    [self reload];
}

- (void)reload {
	NSArray *tempArray = [filteredItems sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
	    NSString *first = [(NSArray*)a objectAtIndex:0];
	    NSString *second = [(NSArray*)b objectAtIndex:0];
	    return [first compare:second];
	}];
	filteredItems = [tempArray mutableCopy];

	[((UIActivityIndicatorView*)[self.view viewWithTag:1]) stopAnimating];

	[self.tableView reloadData];
}

- (void)loadView {
    self.view = [[PWThemableTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain theme:self.theme];
    self.tableView.contentOffset = CGPointMake(0, 44); 
}

- (void)viewWillAppear {
	self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (UITableView *)tableView {
    return (UITableView *)self.view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0; //Should be plenty of room for each account
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [filteredItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    unsigned int row = [indexPath row];

    NSString *identifier = @"AuthyCell";
    PWThemableTableViewCell *cell = (PWThemableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];

    if (!cell) {
        cell = [[PWThemableTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier theme:self.theme];

    	UILabel *refLabel = [[UILabel alloc] initWithFrame:CGRectMake(240.0,5.0,60.0,40.0)];
    	refLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    	refLabel.textColor = [UIColor lightGrayColor];
    	refLabel.alpha = 0;
    	refLabel.text = @"Copied";
    	refLabel.tag = 2;
    	[cell.contentView addSubview:refLabel];
    }
    NSArray *item = [filteredItems objectAtIndex:row];

    //Set textLabel as the first object, which is in the format of account(username)
    cell.textLabel.text = item[1];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:24.0f];

    cell.detailTextLabel.text = item[0];
    cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];

    //Check if the account has an icon
    if (item[2] != nil) {
    	cell.imageView.image = item[2];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    unsigned int row = [indexPath row];
    NSArray *item = [filteredItems objectAtIndex:row];

    //Add the password to the clipboard
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *temp = pasteboard.string;
    if (temp == nil) {
    	temp = @"";
    }
	pasteboard.string = item[1];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	[self performSelector:@selector(resetPB:) withObject:temp afterDelay:10.0 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];

	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	UILabel *refLabel = ((UILabel *)[cell.contentView viewWithTag:2]);
	[UIView animateWithDuration:0.4f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [refLabel setAlpha:1.f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:3.f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [refLabel setAlpha:0.f];
        } completion:nil];
    }];
}

- (void)resetPB:(NSString *)text {
	[UIPasteboard generalPasteboard].string = text;
}

//I hate layouts but this should fit on all devices
- (CGFloat)contentHeightForOrientation:(PWWidgetOrientation)orientation {
	return 360.0;
}
- (CGFloat)contentWidthForOrientation:(PWWidgetOrientation)orientation {
	return 300.0;
}

@end