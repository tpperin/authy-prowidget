@interface App : NSObject {
	NSString* _name;
}
@property(copy, nonatomic) NSString* name;
+(id)loadAppsFromConfig;
-(id)generateOtp;
-(id)sidebarImage;
@end

@interface SBApplication : NSObject
@end

@interface SpringBoard : UIApplication
-(BOOL)isLocked;
-(BOOL)launchApplicationWithIdentifier:(NSString *)identifier suspended:(BOOL)suspended;
@end

@interface SBApplicationController : NSObject
+(instancetype)sharedInstance;
-(SBApplication *)applicationWithDisplayIdentifier:(NSString *)identifier;
@end