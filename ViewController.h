//
//  ViewController.h
//  Authy ProWidget
//
//  Created by Tim Perin on 05.05.2014.
//  Copyright (c) 2014 Tim Perin. All rights reserved.
//

#import "headers.h"
#import "interface.h"
#import <CoreGraphics/CoreGraphics.h>

@interface PWWidgetAuthyViewController : PWContentViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
	NSMutableArray *items;
	NSMutableArray *filteredItems;
}
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSMutableArray *filteredItems;

@end