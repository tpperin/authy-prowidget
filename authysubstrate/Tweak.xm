#import <objc/runtime.h>
#import "../headers.h"
#import "../interface.h"

static inline __attribute__((constructor)) void init() {

	[OBJCIPC registerIncomingMessageFromSpringBoardHandlerForMessageName:@"Authy" handler:^NSDictionary *(NSDictionary *dict) {

		NSString *action = dict[@"action"];

		//User wants to get list of items
		if ([action isEqualToString:@"getList"]) {

			NSMutableArray *apps = [objc_getClass("App") loadAppsFromConfig];

			NSMutableArray *rep = [[NSMutableArray alloc] init];
			//Loop through ALL database items
			for (App *app in apps) {
				//We only want items that have passwords, none of that credit card bullshit
					NSString *name = [app name];
					NSString *code = [app generateOtp];

					UIImage *img = [app sidebarImage];

					[rep addObject:@[name,code,img]];
			}
			return @{@"success":@(YES),@"items":rep};

		} 
		else if ([action isEqualToString:@"refresh"]) {
			NSMutableArray *apps = [objc_getClass("App") loadAppsFromConfig];
			for (App *app in apps) {
				if ([[app name] isEqual:dict[@"name"]]) {
					return @{@"success":@(YES),@"code":[app generateOtp]};
				}
			}
			return @{@"success":@(NO)};
		}
		/*
		else if ([action isEqualToString:@"logout"]) {
			//Lock the profile
			[prof lock];
			return @{@"success":@(YES)};
		}
		*/
		return nil;

	}];
}